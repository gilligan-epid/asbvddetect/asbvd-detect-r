#' Theoretical Risk of not detecting the disease
#' 
#' @description 
#' This function provides the theoretical risk of not detecting the disease for systematic design as a function of
#' \code{N} the number of trees, 
#' \code{m} the number of infected trees in the field,
#' \code{betac} the minimal proportion of leaves with a concentration of RNA that gives a measure below the threshold level 
#' \code{n0} the number of bulks tested 
#' \code{n1} the number of trees sampled for each bulk
#' \code{n2} the number of leaves from each tree
#' @param N an integer
#' @param r a numeric value between 0 and 1
#' @param m an integer between 0 and N
#' @param n0 an integer
#' @param n1 an integer
#' @param n2 an integer
#' @param .beta an integer

risk     <-function(N,r,m=ceiling(r*N),n0,n1,n2,.beta){
  possiblexs<-0:min(n0*n1,m)
  sum(.beta^(n2*possiblexs)*dhyper(x=possiblexs,m=m,n=N-m,k=n0*n1))}


#' Required minimal sample size not to exceed the risk of not detecting the disease 
#' 
#' @description 
#' This function provides the theoretical risk of not detecting the disease for systematic design as a function of
#' \code{N} the number of trees, 
#' \code{m} the number of infected trees in the field,
#' \code{beta} the minimal proportion of leaves with a concentration of RNA that gives a measure below the threshold level 
#' \code{n1} the number of trees sampled for each bulk
#' \code{n2} the number of leaves from each tree
#' \code{.alpha} the accepted risk
#' @returns an integer, the required number of bulks
#' @examples 
#' minimal.n0(6000,0.005,n1=70,n2=1,.beta=.67,.alpha=.95)


minimal.n0<-function(N,r,m=ceiling(r*N),n1,n2,.beta,.alpha){
  n<-0
  risk2<-1
  while(n<N&risk2>.alpha){
    n<-n+1
    risk2<-riskwithoutR(N,m,n0=1,n,n2,.beta)}
  n}


#' Maximal bulk size 
#' 
#' @description 
#' This function provides the theoretical risk of not detecting the disease for systematic design as a function of
#' \code{n2} the number of leaves from each tree
#' \code{risk} the accepted risk
#' @returns an integer, the number of trees present in each bulk
#' @examples 
#' maximal.n1(n2=1,Ct.detect=25,Ct.target=13)
#' maximal.n1(n2=1,Ct.detect=25,Ct.target=13,1.6,4.5,2)

maximal.n1<-function(Ct.detect,
                     Ct.target,
                     n2=1,
                     hat.sigma=1.6,
                     hat.A.upper=4.6,
                     quant=qnorm(.975)){
  floor(10**(hat.A.upper^(-1)*(Ct.detect-Ct.target-hat.sigma*quant))/n2)
}









