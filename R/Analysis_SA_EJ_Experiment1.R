#' Table and graphs for experiment 1 analysis, South African data.
#' 
#' @description 
#' The experiment 1 corresponds to the analysis of 4 x 96 leaves from 4 different trees.
#' @details 
#' graph1
#' @return a list
#' @usage 
#' analysis_SA_EJ_Experiment1()
analysis_SA_EJ_Experiment1<-function(){
  data(SA_EJ_exp1.singles,package="ASBVdDetection")
  require(ggplot2)
  # require(SweaveLst)    
  require(reshape2)
  #table(unique(SA_EJ_exp1.singles[c("tree","leafindex")])$tree)
  X<-SA_EJ_exp1.singles
  library(dplyr)
  XX<- X|>                      
    group_by(tree,topbottom,SWNE,leafindex) |>                         # Specify group indicator
    summarise_at(vars(Ct),      
                 list(minCt = min))   
  
  XX<-XX[order(XX$minCt),]
  XX<-XX |> 
    group_by(tree,topbottom,SWNE) |> 
    mutate(counter = row_number(minCt))
  X<-merge(X,XX,by=c("tree","topbottom","SWNE","leafindex"))
  X<-X |> group_by(tree,topbottom,SWNE,minCt,leafindex,Ct,censored) |> 
    dplyr::mutate(Count = dplyr::n())
  
  
  names(X)[names(X)=="censored"]<-"Censored"
  names(X)[names(X)=="tree"]<-"Tree"
  
  SAexp1dotplot<-ggplot(data = X,
                        mapping = aes(x=counter,
                                      y=Ct,
                                      color=Tree,
                                      shape=Censored,
                                      size=Count))+
    geom_point()+
    facet_grid(topbottom~SWNE)+
    xlab("Leaf")+
    theme(axis.text.x = element_blank(),
          legend.position="bottom",
          axis.ticks = element_blank())+
    scale_size(
      breaks = 1:2,
      labels = 1:2,
      range = c(2, 3))+
    scale_shape_discrete("Censored\\phantom{xxxx}")
  anova(lm(data=SA_EJ_exp1.singles[SA_EJ_exp1.singles$tree==7,],Ct~SWNE))
  anova(lm(data=SA_EJ_exp1.singles[SA_EJ_exp1.singles$tree==7,],Ct~topbottom))
  anova(lm(data=SA_EJ_exp1.singles[SA_EJ_exp1.singles$tree==7,],Ct~SWNE+topbottom))
  anova(lm(data=SA_EJ_exp1.singles[SA_EJ_exp1.singles$tree==7,],Ct~SWNE+topbottom+SWNE*topbottom))
  library(lme4)
  XX<-SA_EJ_exp1.singles
  XX$SWNEtopbottom<-paste0(XX$topbottom,XX$SWNE)
  ll<-lmer(data=XX[XX$tree==7,],formula = 
             Ct~1+(1|SWNEtopbottom))
  summary(ll)
  
  lm(data=SA_EJ_exp1.singles[SA_EJ_exp1.singles$tree==7,],Ct~SWNE*topbottom)
  lm(data=SA_EJ_exp1.singles[SA_EJ_exp1.singles$tree==7,],Ct~SWNE+topbottom+leafindex)
  library(lme4)
  #lme4::lmer(data = SA_EJ_exp1.singles,
  #formula=Ct~(1|tree)+(topbottom|tree)+(topbottom|tree)+(1|leafindex*tree))
  return(list(SAexp1dotplot=SAexp1dotplot))}